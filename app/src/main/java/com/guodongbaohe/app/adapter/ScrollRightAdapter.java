package com.guodongbaohe.app.adapter;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.guodongbaohe.app.R;
import com.guodongbaohe.app.activity.NewSecondClassicActivity;
import com.guodongbaohe.app.bean.ScrollBean;

import java.util.List;

/**
 * Created by Raul_lsj on 2018/3/28.
 */

public class ScrollRightAdapter extends BaseSectionQuickAdapter<ScrollBean, BaseViewHolder> {

    public ScrollRightAdapter(int layoutResId, int sectionHeadResId, List<ScrollBean> data) {
        super(layoutResId, sectionHeadResId, data);
    }

    @Override
    protected void convertHead(BaseViewHolder helper, ScrollBean item) {
        helper.setText(R.id.right_title, item.header);

    }

    @Override
    protected void convert(final BaseViewHolder helper, final ScrollBean item) {
        final ScrollBean.ScrollItemBean t = item.t;
        helper.setText(R.id.right_text, t.getText());
        ImageView imageView = helper.getView(R.id.right_image);
        Glide.with(mContext).load(t.getType()).into(imageView);
        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, NewSecondClassicActivity.class);
                intent.putExtra("name", t.getText());
                intent.putExtra("cate_id", t.getId());
                intent.putExtra("parent_id", t.getP_id());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });
    }


}
