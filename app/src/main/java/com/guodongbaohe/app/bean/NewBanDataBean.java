package com.guodongbaohe.app.bean;

import java.io.Serializable;

public class NewBanDataBean implements Serializable {
    public String title;
    public String image;
    public String url;
    public String extend;
    public String type;
}
