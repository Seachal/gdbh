package com.guodongbaohe.app.bean;

import java.util.List;

public class OrderBean {
    private List<OrderData> result;
    private int rowscount;

    public List<OrderData> getResult() {
        return result;
    }

    public void setResult(List<OrderData> result) {
        this.result = result;
    }

    public int getRowscount() {
        return rowscount;
    }

    public void setRowscount(int rowscount) {
        this.rowscount = rowscount;
    }


    public class OrderData {
        private String adzone_id;
        private String alipay_total_price;
        private String auction_category;
        private String click_time;
        private String commission;
        private String commission_rate;
        private String create_date;
        private String create_time;
        private String income_rate;
        private String item_num;
        private String item_title;
        private String member_id;
        private String num_iid;
        private String order_id;
        private String order_type;
        private String price;
        private String pub_share_pre_fee;
        private String relation_id;
        private String settle_date;
        private String site_id;
        private String special_id;
        private String subsidy_rate;
        private String taobao_uid;
        private String terminal_type;
        private String tk_status;
        private String total_commission_fee;
        private String total_commission_rate;
        private String trade_id;
        private String freeze;
        private String money;
        private String earning_time;

        public String getEarning_time() {
            return earning_time;
        }

        public void setEarning_time(String earning_time) {
            this.earning_time = earning_time;
        }

        public String getFreeze() {
            return freeze;
        }

        public void setFreeze(String freeze) {
            this.freeze = freeze;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getAdzone_id() {
            return adzone_id;
        }

        public void setAdzone_id(String adzone_id) {
            this.adzone_id = adzone_id;
        }

        public String getAlipay_total_price() {
            return alipay_total_price;
        }

        public void setAlipay_total_price(String alipay_total_price) {
            this.alipay_total_price = alipay_total_price;
        }

        public String getAuction_category() {
            return auction_category;
        }

        public void setAuction_category(String auction_category) {
            this.auction_category = auction_category;
        }

        public String getClick_time() {
            return click_time;
        }

        public void setClick_time(String click_time) {
            this.click_time = click_time;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getCommission_rate() {
            return commission_rate;
        }

        public void setCommission_rate(String commission_rate) {
            this.commission_rate = commission_rate;
        }

        public String getCreate_date() {
            return create_date;
        }

        public void setCreate_date(String create_date) {
            this.create_date = create_date;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getIncome_rate() {
            return income_rate;
        }

        public void setIncome_rate(String income_rate) {
            this.income_rate = income_rate;
        }

        public String getItem_num() {
            return item_num;
        }

        public void setItem_num(String item_num) {
            this.item_num = item_num;
        }

        public String getItem_title() {
            return item_title;
        }

        public void setItem_title(String item_title) {
            this.item_title = item_title;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public String getNum_iid() {
            return num_iid;
        }

        public void setNum_iid(String num_iid) {
            this.num_iid = num_iid;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_type() {
            return order_type;
        }

        public void setOrder_type(String order_type) {
            this.order_type = order_type;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPub_share_pre_fee() {
            return pub_share_pre_fee;
        }

        public void setPub_share_pre_fee(String pub_share_pre_fee) {
            this.pub_share_pre_fee = pub_share_pre_fee;
        }

        public String getRelation_id() {
            return relation_id;
        }

        public void setRelation_id(String relation_id) {
            this.relation_id = relation_id;
        }

        public String getSettle_date() {
            return settle_date;
        }

        public void setSettle_date(String settle_date) {
            this.settle_date = settle_date;
        }

        public String getSite_id() {
            return site_id;
        }

        public void setSite_id(String site_id) {
            this.site_id = site_id;
        }

        public String getSpecial_id() {
            return special_id;
        }

        public void setSpecial_id(String special_id) {
            this.special_id = special_id;
        }

        public String getSubsidy_rate() {
            return subsidy_rate;
        }

        public void setSubsidy_rate(String subsidy_rate) {
            this.subsidy_rate = subsidy_rate;
        }

        public String getTaobao_uid() {
            return taobao_uid;
        }

        public void setTaobao_uid(String taobao_uid) {
            this.taobao_uid = taobao_uid;
        }

        public String getTerminal_type() {
            return terminal_type;
        }

        public void setTerminal_type(String terminal_type) {
            this.terminal_type = terminal_type;
        }

        public String getTk_status() {
            return tk_status;
        }

        public void setTk_status(String tk_status) {
            this.tk_status = tk_status;
        }

        public String getTotal_commission_fee() {
            return total_commission_fee;
        }

        public void setTotal_commission_fee(String total_commission_fee) {
            this.total_commission_fee = total_commission_fee;
        }

        public String getTotal_commission_rate() {
            return total_commission_rate;
        }

        public void setTotal_commission_rate(String total_commission_rate) {
            this.total_commission_rate = total_commission_rate;
        }

        public String getTrade_id() {
            return trade_id;
        }

        public void setTrade_id(String trade_id) {
            this.trade_id = trade_id;
        }

        public String getTrade_parent_id() {
            return trade_parent_id;
        }

        public void setTrade_parent_id(String trade_parent_id) {
            this.trade_parent_id = trade_parent_id;
        }

        private String trade_parent_id;
    }
}
