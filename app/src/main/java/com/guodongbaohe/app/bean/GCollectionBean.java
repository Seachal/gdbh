package com.guodongbaohe.app.bean;

import java.util.List;

public class GCollectionBean {

    private List<ResultBean> result;

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        private String attr_price;
        private String attr_prime;
        private String attr_ratio;
        private String attr_site;
        private String cate_category;
        private String cate_route;
        private String coupon_begin;
        private String coupon_final;
        private String coupon_id;
        private String coupon_surplus;
        private String coupon_total;
        private String dateline;
        private String datetime;
        private String goods_gallery;
        private String goods_id;
        private String goods_name;
        private String goods_short;
        private String goods_thumb;
        private String id;
        private String member_id;
        private String sales_month;
        private String seller_shop;
        private String status;
        private boolean ischoosed;
        private String source;

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getAttr_price() {
            return attr_price;
        }

        public void setAttr_price(String attr_price) {
            this.attr_price = attr_price;
        }

        public String getAttr_prime() {
            return attr_prime;
        }

        public void setAttr_prime(String attr_prime) {
            this.attr_prime = attr_prime;
        }

        public String getAttr_ratio() {
            return attr_ratio;
        }

        public void setAttr_ratio(String attr_ratio) {
            this.attr_ratio = attr_ratio;
        }

        public String getAttr_site() {
            return attr_site;
        }

        public void setAttr_site(String attr_site) {
            this.attr_site = attr_site;
        }

        public String getCate_category() {
            return cate_category;
        }

        public void setCate_category(String cate_category) {
            this.cate_category = cate_category;
        }

        public String getCate_route() {
            return cate_route;
        }

        public void setCate_route(String cate_route) {
            this.cate_route = cate_route;
        }

        public String getCoupon_begin() {
            return coupon_begin;
        }

        public void setCoupon_begin(String coupon_begin) {
            this.coupon_begin = coupon_begin;
        }

        public String getCoupon_final() {
            return coupon_final;
        }

        public void setCoupon_final(String coupon_final) {
            this.coupon_final = coupon_final;
        }

        public String getCoupon_id() {
            return coupon_id;
        }

        public void setCoupon_id(String coupon_id) {
            this.coupon_id = coupon_id;
        }

        public String getCoupon_surplus() {
            return coupon_surplus;
        }

        public void setCoupon_surplus(String coupon_surplus) {
            this.coupon_surplus = coupon_surplus;
        }

        public String getCoupon_total() {
            return coupon_total;
        }

        public void setCoupon_total(String coupon_total) {
            this.coupon_total = coupon_total;
        }

        public String getDateline() {
            return dateline;
        }

        public void setDateline(String dateline) {
            this.dateline = dateline;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getGoods_gallery() {
            return goods_gallery;
        }

        public void setGoods_gallery(String goods_gallery) {
            this.goods_gallery = goods_gallery;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_short() {
            return goods_short;
        }

        public void setGoods_short(String goods_short) {
            this.goods_short = goods_short;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public String getSales_month() {
            return sales_month;
        }

        public void setSales_month(String sales_month) {
            this.sales_month = sales_month;
        }

        public String getSeller_shop() {
            return seller_shop;
        }

        public void setSeller_shop(String seller_shop) {
            this.seller_shop = seller_shop;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public boolean isIschoosed() {
            return ischoosed;
        }

        public void setIschoosed(boolean ischoosed) {
            this.ischoosed = ischoosed;
        }

    }
}
