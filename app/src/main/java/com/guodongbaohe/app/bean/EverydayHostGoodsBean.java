package com.guodongbaohe.app.bean;

import java.util.List;

public class EverydayHostGoodsBean {

    public List<GoodsList> getResult() {
        return result;
    }

    public void setResult(List<GoodsList> result) {
        this.result = result;
    }

    private List<GoodsList> result;

    public class GoodsList {
        private String activity_type;
        private String admin_id;
        private String attr_price;
        private String attr_prime;
        private String attr_ratio;
        private String attr_site;
        private String comment;
        private String content;
        private String coupon_begin;
        private String coupon_final;
        private String coupon_url;
        private String dateline;
        private String goods_cate;
        private String goods_gallery;
        private String goods_id;
        private String goods_url;
        private String id;
        private String ip;
        private String status;
        private String timeline;
        private String times;
        private String type;
        private boolean isLogin;
        private String son_count;
        private String source;
        private String video;
        private String goods_comment;
        private String video_cover;
        private String flag;
        private String goods_id_list;
        private String attr_price_list;
        private String member_role;

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getGoods_id_list() {
            return goods_id_list;
        }

        public void setGoods_id_list(String goods_id_list) {
            this.goods_id_list = goods_id_list;
        }

        public String getAttr_price_list() {
            return attr_price_list;
        }

        public void setAttr_price_list(String attr_price_list) {
            this.attr_price_list = attr_price_list;
        }

        public String getVideo_cover() {
            return video_cover;
        }

        public void setVideo_cover(String video_cover) {
            this.video_cover = video_cover;
        }

        public String getGoods_comment() {
            return goods_comment;
        }

        public void setGoods_comment(String goods_comment) {
            this.goods_comment = goods_comment;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public boolean isLogin() {
            return isLogin;
        }

        public void setLogin(boolean login) {
            isLogin = login;
        }

        public String getSon_count() {
            return son_count;
        }

        public void setSon_count(String son_count) {
            this.son_count = son_count;
        }

        public String getMember_role() {
            return member_role;
        }

        public void setMember_role(String member_role) {
            this.member_role = member_role;
        }

        public String getActivity_type() {
            return activity_type;
        }

        public void setActivity_type(String activity_type) {
            this.activity_type = activity_type;
        }

        public String getAdmin_id() {
            return admin_id;
        }

        public void setAdmin_id(String admin_id) {
            this.admin_id = admin_id;
        }

        public String getAttr_price() {
            return attr_price;
        }

        public void setAttr_price(String attr_price) {
            this.attr_price = attr_price;
        }

        public String getAttr_prime() {
            return attr_prime;
        }

        public void setAttr_prime(String attr_prime) {
            this.attr_prime = attr_prime;
        }

        public String getAttr_ratio() {
            return attr_ratio;
        }

        public void setAttr_ratio(String attr_ratio) {
            this.attr_ratio = attr_ratio;
        }

        public String getAttr_site() {
            return attr_site;
        }

        public void setAttr_site(String attr_site) {
            this.attr_site = attr_site;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCoupon_begin() {
            return coupon_begin;
        }

        public void setCoupon_begin(String coupon_begin) {
            this.coupon_begin = coupon_begin;
        }

        public String getCoupon_final() {
            return coupon_final;
        }

        public void setCoupon_final(String coupon_final) {
            this.coupon_final = coupon_final;
        }

        public String getCoupon_url() {
            return coupon_url;
        }

        public void setCoupon_url(String coupon_url) {
            this.coupon_url = coupon_url;
        }

        public String getDateline() {
            return dateline;
        }

        public void setDateline(String dateline) {
            this.dateline = dateline;
        }

        public String getGoods_cate() {
            return goods_cate;
        }

        public void setGoods_cate(String goods_cate) {
            this.goods_cate = goods_cate;
        }

        public String getGoods_gallery() {
            return goods_gallery;
        }

        public void setGoods_gallery(String goods_gallery) {
            this.goods_gallery = goods_gallery;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_url() {
            return goods_url;
        }

        public void setGoods_url(String goods_url) {
            this.goods_url = goods_url;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTimeline() {
            return timeline;
        }

        public void setTimeline(String timeline) {
            this.timeline = timeline;
        }

        public String getTimes() {
            return times;
        }

        public void setTimes(String times) {
            this.times = times;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }
}
