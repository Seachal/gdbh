package com.guodongbaohe.app.bean;

import java.util.List;

public class HomeListBean {

    public List<ListData> getResult() {
        return result;
    }

    public void setResult(List<ListData> result) {
        this.result = result;
    }

    private List<ListData> result;

    public class ListData {
        private String activity;
        private String attr_charge;
        private String attr_detail;
        private String attr_index;
        private String attr_price;
        private String attr_prime;
        private String attr_ratio;
        private String attr_site;
        private String cate_material;
        private String cate_route;
        private String cate_subid;
        private String cate_supid;
        private String coupon_begin;
        private String coupon_final;
        private String coupon_surplus;
        private String coupon_total;
        private String goods_cate;
        private String goods_gallery;
        private String goods_id;
        private String goods_name;
        private String goods_short;
        private String goods_slogan;
        private String goods_tags;
        private String goods_thumb;
        private String id;
        private String sales_hours;
        private String sales_month;
        private String sales_today;
        private String seller_id;
        private String seller_shop;
        private String source;
        private String status;
        private String stick;
        private String timeline;
        private String coupon_explain;
        private String cate_category;
        private boolean isLogin;
        private String son_count;
        private String member_role;
        private String coupon_id;

        public String getCoupon_id() {
            return coupon_id;
        }

        public void setCoupon_id(String coupon_id) {
            this.coupon_id = coupon_id;
        }

        public String getCate_category() {
            return cate_category;
        }

        public void setCate_category(String cate_category) {
            this.cate_category = cate_category;
        }

        public String getCoupon_explain() {
            return coupon_explain;
        }

        public void setCoupon_explain(String coupon_explain) {
            this.coupon_explain = coupon_explain;
        }

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public String getAttr_charge() {
            return attr_charge;
        }

        public void setAttr_charge(String attr_charge) {
            this.attr_charge = attr_charge;
        }

        public String getAttr_detail() {
            return attr_detail;
        }

        public void setAttr_detail(String attr_detail) {
            this.attr_detail = attr_detail;
        }

        public String getAttr_index() {
            return attr_index;
        }

        public void setAttr_index(String attr_index) {
            this.attr_index = attr_index;
        }

        public String getAttr_price() {
            return attr_price;
        }

        public void setAttr_price(String attr_price) {
            this.attr_price = attr_price;
        }

        public String getAttr_prime() {
            return attr_prime;
        }

        public void setAttr_prime(String attr_prime) {
            this.attr_prime = attr_prime;
        }

        public String getAttr_ratio() {
            return attr_ratio;
        }

        public void setAttr_ratio(String attr_ratio) {
            this.attr_ratio = attr_ratio;
        }

        public String getAttr_site() {
            return attr_site;
        }

        public void setAttr_site(String attr_site) {
            this.attr_site = attr_site;
        }

        public String getCate_material() {
            return cate_material;
        }

        public void setCate_material(String cate_material) {
            this.cate_material = cate_material;
        }

        public String getCate_route() {
            return cate_route;
        }

        public void setCate_route(String cate_route) {
            this.cate_route = cate_route;
        }

        public String getCate_subid() {
            return cate_subid;
        }

        public void setCate_subid(String cate_subid) {
            this.cate_subid = cate_subid;
        }

        public String getCate_supid() {
            return cate_supid;
        }

        public void setCate_supid(String cate_supid) {
            this.cate_supid = cate_supid;
        }

        public String getCoupon_begin() {
            return coupon_begin;
        }

        public void setCoupon_begin(String coupon_begin) {
            this.coupon_begin = coupon_begin;
        }

        public String getCoupon_final() {
            return coupon_final;
        }

        public void setCoupon_final(String coupon_final) {
            this.coupon_final = coupon_final;
        }

        public String getCoupon_surplus() {
            return coupon_surplus;
        }

        public void setCoupon_surplus(String coupon_surplus) {
            this.coupon_surplus = coupon_surplus;
        }

        public String getCoupon_total() {
            return coupon_total;
        }

        public void setCoupon_total(String coupon_total) {
            this.coupon_total = coupon_total;
        }

        public String getGoods_cate() {
            return goods_cate;
        }

        public void setGoods_cate(String goods_cate) {
            this.goods_cate = goods_cate;
        }

        public String getGoods_gallery() {
            return goods_gallery;
        }

        public void setGoods_gallery(String goods_gallery) {
            this.goods_gallery = goods_gallery;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_short() {
            return goods_short;
        }

        public void setGoods_short(String goods_short) {
            this.goods_short = goods_short;
        }

        public String getGoods_slogan() {
            return goods_slogan;
        }

        public void setGoods_slogan(String goods_slogan) {
            this.goods_slogan = goods_slogan;
        }

        public String getGoods_tags() {
            return goods_tags;
        }

        public void setGoods_tags(String goods_tags) {
            this.goods_tags = goods_tags;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSales_hours() {
            return sales_hours;
        }

        public void setSales_hours(String sales_hours) {
            this.sales_hours = sales_hours;
        }

        public String getSales_month() {
            return sales_month;
        }

        public void setSales_month(String sales_month) {
            this.sales_month = sales_month;
        }

        public String getSales_today() {
            return sales_today;
        }

        public void setSales_today(String sales_today) {
            this.sales_today = sales_today;
        }

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }

        public String getSeller_shop() {
            return seller_shop;
        }

        public void setSeller_shop(String seller_shop) {
            this.seller_shop = seller_shop;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStick() {
            return stick;
        }

        public void setStick(String stick) {
            this.stick = stick;
        }

        public String getTimeline() {
            return timeline;
        }

        public void setTimeline(String timeline) {
            this.timeline = timeline;
        }

        public boolean isLogin() {
            return isLogin;
        }

        public void setLogin(boolean login) {
            isLogin = login;
        }

        public String getSon_count() {
            return son_count;
        }

        public void setSon_count(String son_count) {
            this.son_count = son_count;
        }

        public String getMember_role() {
            return member_role;
        }

        public void setMember_role(String member_role) {
            this.member_role = member_role;
        }
    }
}
