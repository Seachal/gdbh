package com.guodongbaohe.app.bean;

public class QRCodePicsBean {
    private String url;
    private boolean iswhich;
    private boolean checked;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isIswhich() {
        return iswhich;
    }

    public void setIswhich(boolean iswhich) {
        this.iswhich = iswhich;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}
