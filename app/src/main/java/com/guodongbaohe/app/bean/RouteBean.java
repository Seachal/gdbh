package com.guodongbaohe.app.bean;

import java.util.List;

public class RouteBean {
    public List<RouteData> getResult() {
        return result;
    }

    public void setResult(List<RouteData> result) {
        this.result = result;
    }

    private List<RouteData> result;

    public class RouteData {
        private boolean isLogin;
        private String son_count;
        private String member_role;
        private String attr_charge;
        private String attr_price;
        private String attr_prime;
        private String attr_ratio;
        private String attr_site;
        private String cate_category;
        private String cate_route;
        private String category_id;
        private String category_name;
        private String commission_type;
        private String coupon_begin;
        private String coupon_explain;
        private String coupon_final;
        private String coupon_id;
        private String coupon_share_url;
        private String coupon_surplus;
        private String coupon_total;
        private String goods_gallery;
        private String goods_id;
        private String goods_name;
        private String goods_short;
        private String goods_thumb;
        private String include_dxjh;
        private String include_mkt;
        private String item_url;
        private String level_one_category_name;
        private String provcity;
        private String reserve_price;
        private String sales_month;
        private String seller_id;
        private String seller_shop;
        private String shop_dsr;
        private String tk_total_commi;
        private String tk_total_sales;
        private String url;
        private String user_type;
        private String white_image;
        private String goods_slogan;
        private String source;

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getGoods_slogan() {
            return goods_slogan;
        }

        public void setGoods_slogan(String goods_slogan) {
            this.goods_slogan = goods_slogan;
        }


        public boolean isLogin() {
            return isLogin;
        }

        public void setLogin(boolean login) {
            isLogin = login;
        }

        public String getSon_count() {
            return son_count;
        }

        public void setSon_count(String son_count) {
            this.son_count = son_count;
        }

        public String getMember_role() {
            return member_role;
        }

        public void setMember_role(String member_role) {
            this.member_role = member_role;
        }

        public String getAttr_charge() {
            return attr_charge;
        }

        public void setAttr_charge(String attr_charge) {
            this.attr_charge = attr_charge;
        }

        public String getAttr_price() {
            return attr_price;
        }

        public void setAttr_price(String attr_price) {
            this.attr_price = attr_price;
        }

        public String getAttr_prime() {
            return attr_prime;
        }

        public void setAttr_prime(String attr_prime) {
            this.attr_prime = attr_prime;
        }

        public String getAttr_ratio() {
            return attr_ratio;
        }

        public void setAttr_ratio(String attr_ratio) {
            this.attr_ratio = attr_ratio;
        }

        public String getAttr_site() {
            return attr_site;
        }

        public void setAttr_site(String attr_site) {
            this.attr_site = attr_site;
        }

        public String getCate_category() {
            return cate_category;
        }

        public void setCate_category(String cate_category) {
            this.cate_category = cate_category;
        }

        public String getCate_route() {
            return cate_route;
        }

        public void setCate_route(String cate_route) {
            this.cate_route = cate_route;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getCommission_type() {
            return commission_type;
        }

        public void setCommission_type(String commission_type) {
            this.commission_type = commission_type;
        }

        public String getCoupon_begin() {
            return coupon_begin;
        }

        public void setCoupon_begin(String coupon_begin) {
            this.coupon_begin = coupon_begin;
        }

        public String getCoupon_explain() {
            return coupon_explain;
        }

        public void setCoupon_explain(String coupon_explain) {
            this.coupon_explain = coupon_explain;
        }

        public String getCoupon_final() {
            return coupon_final;
        }

        public void setCoupon_final(String coupon_final) {
            this.coupon_final = coupon_final;
        }

        public String getCoupon_id() {
            return coupon_id;
        }

        public void setCoupon_id(String coupon_id) {
            this.coupon_id = coupon_id;
        }

        public String getCoupon_share_url() {
            return coupon_share_url;
        }

        public void setCoupon_share_url(String coupon_share_url) {
            this.coupon_share_url = coupon_share_url;
        }

        public String getCoupon_surplus() {
            return coupon_surplus;
        }

        public void setCoupon_surplus(String coupon_surplus) {
            this.coupon_surplus = coupon_surplus;
        }

        public String getCoupon_total() {
            return coupon_total;
        }

        public void setCoupon_total(String coupon_total) {
            this.coupon_total = coupon_total;
        }

        public String getGoods_gallery() {
            return goods_gallery;
        }

        public void setGoods_gallery(String goods_gallery) {
            this.goods_gallery = goods_gallery;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_short() {
            return goods_short;
        }

        public void setGoods_short(String goods_short) {
            this.goods_short = goods_short;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getInclude_dxjh() {
            return include_dxjh;
        }

        public void setInclude_dxjh(String include_dxjh) {
            this.include_dxjh = include_dxjh;
        }

        public String getInclude_mkt() {
            return include_mkt;
        }

        public void setInclude_mkt(String include_mkt) {
            this.include_mkt = include_mkt;
        }

        public String getItem_url() {
            return item_url;
        }

        public void setItem_url(String item_url) {
            this.item_url = item_url;
        }

        public String getLevel_one_category_name() {
            return level_one_category_name;
        }

        public void setLevel_one_category_name(String level_one_category_name) {
            this.level_one_category_name = level_one_category_name;
        }

        public String getProvcity() {
            return provcity;
        }

        public void setProvcity(String provcity) {
            this.provcity = provcity;
        }

        public String getReserve_price() {
            return reserve_price;
        }

        public void setReserve_price(String reserve_price) {
            this.reserve_price = reserve_price;
        }

        public String getSales_month() {
            return sales_month;
        }

        public void setSales_month(String sales_month) {
            this.sales_month = sales_month;
        }

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }

        public String getSeller_shop() {
            return seller_shop;
        }

        public void setSeller_shop(String seller_shop) {
            this.seller_shop = seller_shop;
        }

        public String getShop_dsr() {
            return shop_dsr;
        }

        public void setShop_dsr(String shop_dsr) {
            this.shop_dsr = shop_dsr;
        }

        public String getTk_total_commi() {
            return tk_total_commi;
        }

        public void setTk_total_commi(String tk_total_commi) {
            this.tk_total_commi = tk_total_commi;
        }

        public String getTk_total_sales() {
            return tk_total_sales;
        }

        public void setTk_total_sales(String tk_total_sales) {
            this.tk_total_sales = tk_total_sales;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUser_type() {
            return user_type;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public String getWhite_image() {
            return white_image;
        }

        public void setWhite_image(String white_image) {
            this.white_image = white_image;
        }

        public String getZk_final_price() {
            return zk_final_price;
        }

        public void setZk_final_price(String zk_final_price) {
            this.zk_final_price = zk_final_price;
        }

        private String zk_final_price;
    }
}
