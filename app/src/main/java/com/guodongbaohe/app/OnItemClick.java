package com.guodongbaohe.app;

import android.view.View;

public interface OnItemClick {
    void OnItemClickListener(View view, int position);
}
